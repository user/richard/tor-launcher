<!ENTITY torsettings.dialog.title "Настройки сети Tor">
<!ENTITY torsettings.wizard.title.default "Подключение к Tor">
<!ENTITY torsettings.wizard.title.configure "Настройки сети Tor">
<!ENTITY torsettings.wizard.title.connecting "Идет подключение">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "Язык Tor Browser">
<!ENTITY torlauncher.localePicker.prompt "Пожалуйста, выберите язык.">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "Нажмите &quot;Соединиться&quot; для подключения к Tor.">
<!ENTITY torSettings.configurePrompt "Нажмите &quot;Настроить&quot; для настройки сети. Это имеет смысл, если вы в стране, запрещающей Tor (например, Египет, Китай, Турция), или если вы подключаетесь из приватной сети, требующей прокси.">
<!ENTITY torSettings.configure "Настроить">
<!ENTITY torSettings.connect "Соединиться">

<!-- Other: -->

<!ENTITY torsettings.startingTor "Ожидание запуска Tor...">
<!ENTITY torsettings.restartTor "Перезапустить Tor">
<!ENTITY torsettings.reconfigTor "Изменить настройки">

<!ENTITY torsettings.discardSettings.prompt "Вы настроили мосты Tor или локальный прокси-сервер. Для прямого подключения к сети Tor эти параметры нужно удалить.">
<!ENTITY torsettings.discardSettings.proceed "Удалить настройки и подключиться">

<!ENTITY torsettings.optional "Дополнительно">

<!ENTITY torsettings.useProxy.checkbox "Я использую прокси для подключения к интернету">
<!ENTITY torsettings.useProxy.type "Тип прокси">
<!ENTITY torsettings.useProxy.type.placeholder "выбор типа прокси">
<!ENTITY torsettings.useProxy.address "Адрес">
<!ENTITY torsettings.useProxy.address.placeholder "IP-адрес или хост">
<!ENTITY torsettings.useProxy.port "Порт">
<!ENTITY torsettings.useProxy.username "Имя пользователя">
<!ENTITY torsettings.useProxy.password "Пароль">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "Мой брандмауэр разрешает подключения только к определенным портам">
<!ENTITY torsettings.firewall.allowedPorts "Разрешенные порты">
<!ENTITY torsettings.useBridges.checkbox "Tor заблокирован в моей стране">
<!ENTITY torsettings.useBridges.default "Выбрать встроенный мост">
<!ENTITY torsettings.useBridges.default.placeholder "выбор моста">
<!ENTITY torsettings.useBridges.bridgeDB "Запросить мост у torproject.org">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "Введите символы с изображения">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "Показать другую картинку">
<!ENTITY torsettings.useBridges.captchaSubmit "ОК">
<!ENTITY torsettings.useBridges.custom "Указать мост вручную">
<!ENTITY torsettings.useBridges.label "Укажите данные моста из доверенного источника">
<!ENTITY torsettings.useBridges.placeholder "адрес:порт (по одному в строке)">

<!ENTITY torsettings.copyLog "Скопировать журнал Tor в буфер обмена">

<!ENTITY torsettings.proxyHelpTitle "Помощь по прокси">
<!ENTITY torsettings.proxyHelp1 "Локальный прокси-сервер может понадобиться при подключении через корпоративную, школьную или университетскую сеть.&#160;Если вы не уверены, нужен ли прокси, посмотрите сетевые настройки в другом браузере или проверьте сетевые настройки вашей системы.">

<!ENTITY torsettings.bridgeHelpTitle "Помощь по мостам (ретрансляторам)">
<!ENTITY torsettings.bridgeHelp1 "Мосты – непубличные точки-посредники (ретрансляторы), которые затрудняют попытки цензоров блокировать подключения к сети Tor. Каждый тип моста использует отличный от других метод для обхода блокировки. Мосты типа &quot;obfs&quot; делают ваш трафик похожим на случайный шум. Мосты типа &quot;meek&quot; имитируют подключение к сервису, отличному от Tor.">
<!ENTITY torsettings.bridgeHelp2 "Разные страны по-разному пытаются блокировать Tor. Поэтому те или иные мосты работают в одних странах, но не работают в других. Если не уверены в том, какой мост сработает у вас в стране, посетите torproject.org/about/contact.html#support">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "Пожалуйста, подождите, пока мы установим подключение к сети Tor. Это может занять несколько минут.">

<!-- #31286 about:preferences strings  -->
<!ENTITY torPreferences.categoryTitle "Tor">
<!ENTITY torPreferences.torSettings "Настройки Tor">
<!ENTITY torPreferences.torSettingsDescription "Tor Browser перенаправляет ваш трафик через сеть Tor. Ее поддерживают тысячи добровольцев по всему миру." >
<!ENTITY torPreferences.learnMore "Подробнее">
<!ENTITY torPreferences.quickstart "Quickstart">
<!ENTITY torPreferences.quickstartDescription "Quickstart allows Tor Browser to connect automatically.">
<!ENTITY torPreferences.quickstartCheckbox "Always connect automatically">
<!ENTITY torPreferences.bridges "Мосты">
<!ENTITY torPreferences.bridgesDescription "Мосты помогают получить доступ к сети Tor там, где он заблокирован. В зависимости от вашего местонахождения один мост может работать лучше другого.">
<!ENTITY torPreferences.useBridge "Использовать мост">
<!ENTITY torPreferences.requestNewBridge "Запросить новый мост…">
<!ENTITY torPreferences.provideBridge "Указать свой мост">
<!ENTITY torPreferences.advanced "Дополнительно">
<!ENTITY torPreferences.advancedDescription "Настройте подключение Tor Browser к интернету.">
<!ENTITY torPreferences.firewallPortsPlaceholder "Значения, разделенные запятыми">
<!ENTITY torPreferences.requestBridgeDialogTitle "Запрос моста">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "Обращение к BridgeDB. Пожалуйста, подождите.">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "Решите CAPTCHA для запроса моста">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "Неправильно. Пожалуйста, попробуйте снова.">
<!ENTITY torPreferences.viewTorLogs "Просмотр журнала Tor">
<!ENTITY torPreferences.viewLogs "Смотреть журнал…">
<!ENTITY torPreferences.torLogsDialogTitle "Журнал Tor">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.tryAgain "Переподключение">
<!ENTITY torConnect.offline "Отключён">
<!ENTITY torConnect.connectMessage "Изменения в настройках Tor не вступят в силу, пока вы не подключитесь к сети Tor">
<!ENTITY torConnect.tryAgainMessage "Браузеру Tor не удалось установить соединение с сетью Tor">
<!ENTITY torConnect.connectingConcise "Соединение...">
<!ENTITY torConnect.connectedConcise "Подключено">
<!ENTITY torConnect.copyLog "Копирование журналов Tor">
